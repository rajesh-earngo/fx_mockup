var util = require('util');
var rest = require('restler');

var backendCtrl = {


    // set values to respose
    sendResponse: function (res, code, status, data) {
        res.status(code);
        res.json({
            'status': status,
            'data': data
        });
    },


    /*
    Using REST call, get the live update of Currency Value
    */
    getCurrencyRate: function (req, res) {
        console.log('[backendCtrl] Get Notification message_query = ' + util.inspect(req.query));

        var pair = req.query.pair;

        var apiUrl = 'http://free.currencyconverterapi.com/api/v3/convert?q=' + req.query.pair + '&compact=y';

        rest.get(apiUrl).on('complete', function (data) {
            console.log('[backendCtrl] Data : ' + util.inspect(data)); // auto convert to object

            console.log('[backendCtrl] Data : ' + util.inspect(data[pair].val));
            backendCtrl.sendResponse(res, 200, 'SUCCESS', data[pair].val);
        });

    }, // end of get CurrencyRate
}

module.exports = backendCtrl;