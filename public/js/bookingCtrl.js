var bookingModule = angular.module('fxMockApp.booking', ['ngMessages']);


bookingModule.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/booking/', {
        templateUrl: 'pages/booking.html',
    });
}]);



bookingModule.controller('bookingController', function ($scope, pricingService) {

    console.log('bookng controller');

    $scope.booking = new Object();
    $scope.booking.txtOptionType = '';
    $scope.booking.txtSymbol = '';
    $scope.booking.txtOptionValue = '';
    $scope.booking.txtDelta = '';
    $scope.booking.txtGamma = '';
    $scope.booking.txtCurrencyPair = '';
    $scope.booking.isCalculationDone = false;

    //fields same as pricing
    $scope.booking = pricingService.getPricingDetail();


    // submit the booking 
    $scope.submitBooking = function () {

        var flag = $scope.booking.isCalculationDone || false;
        if (flag == false || $scope.booking.isCalculationDone == false) {
            bootbox.alert("Pricing is not calculated.");
            return false;
        } else {
            bootbox.alert("Submiting information for Booking. Demo stops here.");
        }
    };
});