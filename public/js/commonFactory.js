/**
 * Common methods used in failed_form handling 
 */

angular.module('fxMockApp').factory("commonFactory", function ($http) {

    return {
        
        // for Get live Currency Rate
        getConversionRate: function (currencyFrom, currencyTo) {

            currencyFrom = 'USD';
            currencyTo = 'SGD';

            $http.get('/api/v1/currency', {
                    params: {
                        pair: currencyFrom + '_' + currencyTo
                    }
                })
                .success(function (data) {
                    console.log('Exchange Rate : ' + data.val);
                })
                .error(function (data) {
                    console.log('Error: ' + data);
                });


            return 0;
        }, // end of getConversionRate



        // for Get live Currency Rate
        getConversionRate: function (pairValue, callback) {

            $http.get('/api/v1/currency', {
                    params: {
                        pair: pairValue
                    }
                })
                .success(function (data) {
                    console.log('Exchange Rate : ' + data.val);
                    callback('', data);
                })
                .error(function (err) {
                    console.log('Error: ' + err);
                    callback(err, '');
                });
        }, // end of getConversionRate

    };
});