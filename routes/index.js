var express = require('express');
var router = express.Router();

var backendCtrl = require('../controller/backendCtrl.js');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {
        title: 'Express'
    });
});


router.get('/api/v1/currency', backendCtrl.getCurrencyRate);


module.exports = router;